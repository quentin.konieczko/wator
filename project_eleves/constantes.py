#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
Constantes de WA-TOR
'''

##############################################################
#####                                                    #####
#####                     CONSTANTES                     #####
#####                                                    #####
##############################################################

LARGEUR_MER =  # la largeur de la grille
# la hauteur de la grille
ENERGIE_REQUIN =  # l'énergie initiale d'un requin

TEMPS_GESTATION = {"thon": 2, "requin": 5}  # les temps de gestation

DIRECTIONS = ((0, -1), (0, 1), (1, 0), (-1, 0))  # N, S, E, O
