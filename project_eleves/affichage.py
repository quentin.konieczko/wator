#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
Fonctions employées pour l'affichage du jeu (console et graphique)

* caractereBoard : associe à chaque cellule de la grille un caractère
* genererBoard : génère une chaîne de caractères affichable dans la console
    représentant la mer
* afficherMer : affiche l'état de la mer (board et effectifs) dans la console
* compterPoissons : renvoie les effectifs de chaque espèce dans la mer
* construireGraphique : affiche l'évolutions des effectifs avec pylab

'''


import pylab
from pickle import dump, load
from constantes import HAUTEUR_MER, LARGEUR_MER

##############################################################
#####                                                    #####
#####        AFFICHAGE CONSOLE ET GRAPHIQUE              #####
#####                                                    #####
##############################################################


def caractereBoard(poisson):

    if poisson is None:
        return ' '
    elif poisson["type"] == "thon":
        return 'T'
    return 'R'


def genererBoard(grid):
    '''
    Renvoie une chaîne de caractère représentant la mer.

    :param grid: (list of list) la mer
    :return: (str) board, la chaîne de caractère sur plusieurs lignes présentant
    la mer.
    '''
    board = ''
    # on a besoin des coordonnées donc il faut itérer sur celles-ci
    for ord in range(HAUTEUR_MER):
        # pour chaque ligne
        for abs in range(LARGEUR_MER):
            # on attribue à chaque poisson son caractère
            char = caractereBoard(grid[ord][abs])
            board += char + ' '
        board += '\n'
    return board


def afficherMer(grid, nb_thons, nb_requins, step):

    board = genererBoard(grid)
    print(board)
    print("\nEtape {}, {} thons et {} requins\n".format(step, nb_thons,
                                                        nb_requins))


def compterPoissons(grid):

    pass


def construireGraphique(table_steps, table_thons, table_requins):
    '''
    Affiche le graphique de la simulation avec pylab

    :param table_steps: le tableau des pas (absisses)
    :param table_steps: le tableau des effectifs des thons (ord, bleu)
    :param table_steps: le tableau des effectifs des requins (ord, orange)
    :return: None
    :CU: affiche un message et construit un graphique et l'affiche avec pylab
    '''
    pass


def sauvegarderData(table_steps, table_thons, table_requins):
    '''
    Fonction utilitaire servant à tester l'affichage de données.
    N'est utilisée que pour développer le projet.

    :param table_steps: (list) liste des pas
    :param table_thons: (list) liste des effectifs des thons
    :param table_requins: (list) liste des effectifs des requins
    :return: (None)
    :SE: écrit les trois listes dans les fichiers tables_steps, tables_thons
        et table_requins
    '''
    # on sauvegarde les données à l'aide du module pickle dans trois fichiers.
    with open('./donnees_exemples/table_steps', 'wb') as f:
        dump(table_steps, f)

    with open('./donnees_exemples/table_thons', 'wb') as f:
        dump(table_thons, f)

    with open('./donnees_exemples/table_requins', 'wb') as f:
        dump(table_requins, f)


def construireGraphiqueSimule():
    '''
    Fonction utilitaire permettant de tester les graphiques simulés.

    Récupère les données de trois fichiers et appelle la fonction
    construireGraphique pour afficher les données.

    :return: None
    :SE: charge trois fichiers puis affiche un graphique.
    '''
    # on charge les données depuis les trois fichiers.
    # le module pickle permet de convertir directement en liste.
    with open('./donnees_exemples/table_steps', 'rb') as f:
        table_steps = load(f)

    with open('./donnees_exemples/table_thons', 'rb') as f:
        table_thons = load(f)

    with open('./donnees_exemples/table_requins', 'rb') as f:
        table_requins = load(f)

    construireGraphique(table_steps, table_thons, table_requins)


if __name__ == '__main__':
    # simulation de construction du graphique avec des données fournies
    construireGraphiqueSimule()
