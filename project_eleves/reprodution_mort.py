#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
Fonctions utilisées pour la reproduction et la mort d'un poisson

* reproduction : effectue la reproduction complète d'un poisson (gestation,
    apparition d'un nouveau poisson)
* mortDuRequin : tue le requin d'énergie nulle.

'''

from constantes import TEMPS_GESTATION
from generation_mer import creerThon, creerRequin


##############################################################
#####                                                    #####
#####   REPRODUCTION D'UN POISSON ET MORT D'UN REQUIN    #####
#####                                                    #####
##############################################################


def reproduction(grid, abs, ord, nlle_abs, nlle_ord, deplacement_possible):
    '''
    La reproduction du poisson.
    Renvoie les nouveaux états après calcul.
    Si le temps de gestation est arrivé à 0 et qu'il s'est déplacé,
    le poisson se reproduit.

    '''

    return


def mortDuRequin(grid, poisson, nlle_abs, nlle_ord):
    '''
    Tue le requin dépourvu d'énergie
    Si le requin n'a plus d'énergie on le tue. Sa case devient un None.

    :param grid: (list of list)
    :param poisson: (dict) le poisson
    :param nlle_abs: (int) l'absisse d'arrivée du poisson
    :param nlle_ord: (int) l'ordonnée d'arrivée du poisson
    :return: None
    :SE: modifie grid et poisson en place
    '''
    pass
