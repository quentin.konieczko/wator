#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
Fonction utilisée pour le calcul d'une génération suivante.

* generationSuivante

'''

from random import randint
from constantes import HAUTEUR_MER, LARGEUR_MER
from deplacement import deplacement
from reprodution_mort import mortDuRequin, reproduction

##############################################################
#####                                                    #####
#####                 GÉNÉRATION SUIVANTE                #####
#####                                                    #####
##############################################################


def generationSuivante(grid):
    '''
    Calcule la génération suivante

    :param grid: (list of list) le tableau contenant la mer
    :return None:
    :CU: modifie le tableau grid en place

    On choisit une case au hasard,
    si elle est vide on ne fait rien
    sinon on applique le comportement correspondant
    '''
    # 1. on choisit une case au hasard
    abs = randint(0, LARGEUR_MER - 1)
    ord = randint(0, HAUTEUR_MER - 1)

    poisson = grid[ord][abs]
    if poisson is None:
        # la case est vide il ne se passe rien, on renvoie
        return

    # arrivé ici, la case contient un poisson, on applique le comportement

    # 2. on calcule le déplacement du poisson
    nlle_abs, nlle_ord, deplacement_possible = deplacement(grid, abs, ord)

    # 3. Requins : énergie et mort du requin si celle-ci est nulle.
    mortDuRequin(grid, poisson, nlle_abs, nlle_ord)

    # 4. le poisson toujours vivant cherche à se reproduire.
    if grid[nlle_ord][nlle_abs] is not None:
        reproduction(grid, abs, ord, nlle_abs, nlle_ord, deplacement_possible)

    # les calculs sont terminés on quitte
