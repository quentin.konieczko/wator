# Projet version élèves

## Les fichiers

Ce dossier contient les fichiers qui seront remis aux élèves.

Ils sont de 4 types 

1. Des illustrations dans **fig**
2. Des fichiers python du type **affichage.py**
Ce sont les sources que les élèves devront modifier, enrichir, compléter 
ou documenter
3. Des fichiers python du type **test_affichage.py**
Ce sont des tests unitaires complets qu'ils peuvent executer. Chaque fonction
étant testée ils pourront réaliser si leur code fonctionne sans dépendre du 
reste
4. Un dossier **donnees_exemples** qui contient différentes données servant aux
tests unitaires.

Bien-sûr, aucun test ne réussit actuellement. Les élèves doivent d'abord 
modifier les sources.

## Partage des documents

En pratique j'opterai sûrement pour une solution de partage collective comme
google classroom. Nous avons mis ça en place dans mon lycée il y a quelques 
années et les élèves parviennent à s'en servir.

Dans ce cas, ils pourraient être amenés à travailler directement dans google 
colab (Jupyter Notebook version google)

Il leur suffira donc de partager un dossier commun et de travailler dedans.


* **Avantages :** moins complexe qu'un git, plus facile de surveiller l'avancée
* **Inconvénients :** pas de vrais contrôle de versions.