#!/usr/bin/env python
# -*- coding: utf-8 -*-

import unittest
import io
import sys
from affichage import *


class TestAffichage(unittest.TestCase):

    def test_caractereBoard(self):
        self.assertEqual(caractereBoard(None), ' ')
        self.assertEqual(caractereBoard({
            "type": "thon", "gestation": 2
        }), 'T')
        self.assertEqual(caractereBoard({
            "type": "requin", "gestation": 2,
            "energie": 3
        }), 'R')

    def test_genererBoard(self):
        grid = [
            [None,
             {
                 "type": "thon", "gestation": 3
             },
                {
                 "type": "requin", "gestation": 3,
                 "energie": 2
             },
                {
                 "type": "requin", "gestation": 3,
                 "energie": 2
             }] * 5
        ] * 20

        board = genererBoard(grid)

        with open('./donnees_exemples/generer_board.txt', mode='r') as f:
            affichage_mer = ''
            lines = f.readlines()
            for line in lines:
                affichage_mer += line

        self.assertEqual(board, affichage_mer)

    def test_afficherMer(self):
        grid = [
            [None,
             {
                 "type": "thon", "gestation": 3
             },
                {
                 "type": "requin", "gestation": 3,
                 "energie": 2
             },
                {
                 "type": "requin", "gestation": 3,
                 "energie": 2
             }] * 5
        ] * 20
        affichage_mer = ''

        capturedOutput = io.StringIO()                 # Create StringIO object
        sys.stdout = capturedOutput                    # and redirect stdout.
        afficherMer(grid, 20, 40, 13)                 # Call function.
        sys.stdout = sys.__stdout__                    # Reset redirect.
        # print(capturedOutput.getvalue())   # Now works as before.
        with open('./donnees_exemples/afficher_mer.txt', mode='r') as f:
            affichage_mer = ''
            lines = f.readlines()
            for line in lines:
                affichage_mer += line
        # print(affichage_mer)
        self.assertEqual(capturedOutput.getvalue(), affichage_mer)

    def test_compterPoissons(self):
        grid = [
            [None,
             {
                 "type": "thon", "gestation": 3
             },
                {
                 "type": "requin", "gestation": 3,
                 "energie": 2
             },
                {
                 "type": "requin", "gestation": 3,
                 "energie": 2
             }] * 5
        ] * 20
        self.assertEqual(compterPoissons(grid), (100, 200))


if __name__ == '__main__':
    unittest.main()
