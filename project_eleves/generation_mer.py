#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
Fonctions permettant la création de la mer

creerMerVide : initialise la mer avec des None
creerThon : renvoie le dictionnaire d'un thon
creerRequin : renvoie le dictionnaire d'un requin
verifierProbabilites : s'assure que les probabilités des thons et requins sont
cohérentes
creerMer : regroupe le tout. Initilise une mer vide et la peuple aléatoirement
selon les probabilités.
'''

from constantes import *
from random import random

##############################################################
#####                                                    #####
#####               GÉNÉRATION DE LA MER                 #####
#####                                                    #####
##############################################################


def creerMerVide():
    '''
    renvoie une grille vide contenant des None de largeur et hauteur données
    :param largeur: (int) largeur de la grille
    :param hauteur: (int) hauteur de la grille
    :return: (list) une liste 2D de largeur et hauteur données
    '''
    return


def creerRequin():
    '''
    Renvoie un dictionnaire contenant les données d'un requin
    :return: (dict) un requin
    '''
    return {
        "type": "requin", "gestation": TEMPS_GESTATION["requin"],
        "energie": ENERGIE_REQUIN
    }


def creerThon():
    return


def verifierProbabilites(proba_thon, proba_requin):
    '''
    Vérifie la confirmité des probabilités données.
    Elles doivent être entre 0 et 1 et leur somme ne peut dépasser 1.
    Sinon on lève une exception ValueError

    :param proba_thon: (int, float) une probabilité
    :param proba_requin: (int, float) une probabilité
    :return: None
    :SE: lève une exception si les conditions ne sont pas remplies.
    '''
    if not isinstance(proba_thon, (int, float)) \
            or not isinstance(proba_requin, (int, float)):
        raise ValueError("les probabilités doivent être des nombres")
    if proba_thon + proba_requin > 1:
        raise ValueError("la somme des probas doit etre < 1")


def creerMer(proba_thon, proba_requin):
    '''
    Crée le tableau initial
    renvoie une table contenant des requins et des thons ou rien
    la proportion de requins et de thons est donnée par les parametres

    :param proba_thon: (float) entre 0 et 1 inclus
    :param proba_requin: (float) entre 0 et 1 inclus
                La somme de deux probas ne peut dépasser 1
    :return: (list of list)
    '''
    # on vérifie la conformité des probabilités

    # on crée une mer vide

    # on la remplit de poissons

    return grid
