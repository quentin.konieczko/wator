#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
'''

from time import sleep

from generation_mer import creerMer
from affichage import compterPoissons, afficherMer, construireGraphique
from generation_suivante import generationSuivante

##############################################################
#####                                                    #####
#####                     SIMULATION                     #####
#####                                                    #####
##############################################################


def simulation(delai=0.1, freq_affichage=10, total=100000,
               proba_thon=0.3, proba_requin=0.1):
    """
    Simule le comportement dans une mer.
    les nombres de requins et de thons sont calculés à partir des probas
    On continue jusqu'à atteindre total.
    Si total == inf, on s'arrête quand une des espèces disparait.

    A chaque étape les effectifs sont enregistrées dans des tableaux internes.

    En fin de simulation les effectifs sont affichés

    :param delai: (float) delai d'affichage avant de reprendre les calculs.
    :param freq_affichage: (int) on affiche à cette fréquence le tableau.
    :param total: (int ou inf), on continue jusqu'à ce nombre de générations
    :param proba_thon: (float) proportion de thons dans la mer initiale
    :param proba_requins: (float) proportion de requins dans la mer initiale
    :return: None
    """
    # on initialise les éléments
    mer = creerMer(proba_thon, proba_requin)
    nombre_thons, nombre_requins = compterPoissons(mer)

    # on crée les tableaux et variables suivant l'avancée
    step = 0
    table_steps = []  # tableau des étapes
    # tableau des effectifs des thons

    # on continue jusqu'à disparition d'une espèce ou jusqu'à
    # atteindre le nombre d'étapes
    while True:

        # on remplit les tableaux qui présenteront les résultats
        table_steps.append(step)

        if step % freq_affichage == 0:
            # on affiche dans la console tous les "freq_affichage"
            afficherMer(mer, nombre_thons, nombre_requins, step)
            sleep(delai)

        # calculer la génération suivante
        generationSuivante(mer)
        # compter les effectifs
        nombre_thons, nombre_requins = compterPoissons(mer)
        # incrémenter les étapes

    construireGraphique(table_steps, table_thons, table_requins)


if __name__ == '__main__':
    # Affichage tous les 1000 étapes pour une mer "standard" jusqu'à disparition
    # d'une espèce ou 1000000 étapes

    simulation(freq_affichage=1000, total=100000)
