---
header-includes:
    - '\defaultfontfeatures{Extension = .otf}'
title:
- WA-TOR
author:
- QK
theme:
- metropolis
---

# WA-TOR

## Le projet WA-TOR

Le projet WA-TOR est une simulation d'un phénomène proies-prédateurs.

<!-- %fig1 bassin requin thon - OK -->
![ la mer](./wator_diapos/fig1-requin-thon-mer.jpg){width=80%}

## Les règles du jeu

* Des thons et des requins habitent une mer à deux dimensions en forme de tore découpée en cases. Chaque case ne peut recevoir qu'un poisson au plus.
* La simulation se fait étape par étape. On appelle génération l'état de la mer à une étape.

<!-- %fig2 extrait console de la mer - OK -->
![ L'affichage de la mer dans la console](./wator_diapos/fig2-console_simulation.png){width=40%}


## Génération suivante.

À chaque étape on choisit aléatoirement une case.

Trois cas :

* La case est **vide** : il ne se passe rien.
* Elle est occupée par un **thon**.
  1. Il se déplace vers une case vide (si possible)
  2. Il cherche à se reproduire.
* Elle est occupée par un **requin**.
  1. Il chasse un thon (si possible), sinon se déplace (si possible).
  2. Les requins s'épuisent et doivent manger. S'il n'a pas mangé depuis trop longtemps il meurt et disparaît.
  3. S'il est toujours vivant, il cherche à se reproduire.

## La mer est un TORE

![tore](https://upload.wikimedia.org/wikipedia/commons/thumb/4/47/Torus.svg/500px-Torus.svg.png){width=200px}

La mer est représentée par un tableau à deux dimensions dans lesquels les bords de gauche et de droite sont joints ainsi que les bords du haut et du bas.


## La mer est un TORE

Un poisson qui quitte la mer par la droite réapparaît par la gauche.

<!-- %fig4 tableau avec les flèches ok -->
![ mer torique](./wator_diapos/fig4-tableau-fleches.svg){width=50%}

# Proies-prédateurs

## Proie prédateur

Les phénomènes **proie-prédateurs** apparaissent de la façon suivante :

1. Si les thons ne sont pas chassés, ils se reproduisent vite.
2. Et alors les requins peuvent chasser souvent et se reproduisent vite à leur tour...
3. Les requins deviennent trop nombreux et tuent beaucoup de thons.
4. L'effectif de thons chute !
5. Les requins n'ont plus rien à chasser et meurent.
6. Et alors les thons ne sont pas chassés... etc.

## Proie prédateur

<!-- %fig5 - pylab proie prédateur - ok -->
![Les effectifs des thons (bleu) et requins (orange) fluctuent](https://gitlab-fil.univ-lille.fr/quentin.konieczko/portail/raw/master/bloc1/wator/evolution.png){width=60%}


Voilà le type de graphe final que vous allez produire.
Cela demande des données précises.

# Objectifs de la simulation

## Objectifs de la simulation

**Vous allez programmer cette simulation en Python.**

L'objectif est de produire un programme complet :

* création et peuplement de la mer
* calcul d'une génération suivante
* affichages :
  * régulièrement dans la console : l'état de la mer (positions et effectifs des poissons)
  * final un graphique présentant l'évolution des populations.

# Les règles

## Les règles précisément - case vide

* **La mer est torique**, de dimensions données.
* **Chaque case** peut recevoir **0 ou 1 poisson**.
* On l'initialise avec des probabilités données.
* A chaque étape **on tire une case au hasard**...

### Si elle est vide on passe.

## Les règles précisément - le thon

### Si c'est un thon, on applique son comportement

* Il **se déplace** vers **une case vide** parmi ses voisines **(N, S, E, O)**
* Sa durée de gestation diminue de 1.
* **Si sa durée de gestation arrive à 0**, elle est réinitialisée.
* Dans ce cas et s'il s'est déplacé, un nouveau thon apparaît dans la case qu'il a abandonné.

## Les règles précisément - le requin

### Si c'est un requin.

* Il **perd un point d'énergie**.
* Il **chasse** un thon parmi ses voisins. Il récupère alors toute son énergie.
* S'il n'a pas pu chasser, il se déplace dans une case vide parmi ses voisins.
* **Si son énergie est nulle, il meurt.**
* Sa durée de gestation diminue de 1.
* Si sa durée de gestation arrive à 0, elle est réinitialisée.
* Dans ce cas et s'il s'est déplacé ou s'il a chassé, un nouveau requin apparaît dans la case qu'il a abandonné.

## Données numériques

* **Dimensions** : 20 x 20. On pourra diminuer pour développer
* **Thons** : gestation 2
* **Requins** : énergie 3, gestation 5
* **Nombre d'étapes** pour voir apparaître un phénomène proie-prédateur : 10.000 au moins
* **Mer initiale** : 30% des cases sont des thons, 10% des cases sont des  requins. Le reste est vide.

# Quelques exemples

## Quelques exemples plus précis - case vide

<!-- %fig6 case vide - ok -->
![ depuis une case vide](./wator_diapos/fig6-exemples-case-vide.svg){width=70%}

## Quelques exemples plus précis - déplacement du thon

<!-- %fig7 déplacement d'un thon - ok -->
![ Le thon se déplace vers une case vide](./wator_diapos/fig7-exemples-deplacement-thon.svg){width=70%}

Le requin qui n'a pas de thon parmi ses voisins se déplace de la même manière

## Quelques exemples plus précis - chasse du requin

<!-- %fig8 chasse du requin - OK -->
![ Le requin cherche à chasser un thon voisin](./wator_diapos/fig8-exemples-chasse-requin.svg){width=70%}

## Quelques exemples plus précis - reproduction d'un poisson

<!-- %fig9 reproduction - ok -->
![ Le poisson se reproduit s'il s'est déplacé](./wator_diapos/fig9-exemples-reproduction.svg){width=70%}

# Vue d'ensemble

## Initialisation et première évolution

<!-- %fig10 mer initiale -(Thon qui se déplace)> étape 1 -->
![ Vue d'ensemble - un thon se déplace](./wator_diapos/fig10-console.png){width=100%}


# Structure du projet

## le dossier du projet

Les fichiers à produire :

* **wa_tor.py** : le module principal qui dirige la simulation
* **generation_mer.py** : création de la mer et des poissons
* **affichage.py** : affichage de la mer dans la console et graphiques finaux
* **deplacement.py** : déplacement et chasse
* **reproduction_mort.py** : reproduction des poissons, mort du requin
* **constantes.py** : les constantes du jeu
* **tests** : chaque fichier est accompagné de tests (fournis)

## Extrait de reproduction_mort.py

~~~{#mycode .python .numberLines startFrom="60"}
def mortDuRequin(grid, poisson, nlle_abs, nlle_ord):
    '''
    Tue le requin dépourvu d'énergie
    Si le requin n'a plus d'énergie on le tue.
    Sa case devient un None.

    :param grid: (list of list)
    :param poisson: (dict) le poisson
    :param nlle_abs: (int) l'absisse d'arrivée du poisson
    :param nlle_ord: (int) l'ordonnée d'arrivée du poisson
    :return: None
    :SE: modifie grid et poisson en place
    '''
~~~

# Tests unitaires

## un test unitaire  qui échoue

```{#mycode .python}
19
12

- [0, 0, 1, 19]
?            ^

+ [0, 0, 1, 12]
?            ^
----------------------------------------------------------------------
Ran 9 tests in 0.001s

FAILED (failures=1)
```

## un test unitaire qui réussi

```{#mycode .python}
.........
----------------------------------------------------------------------
Ran 9 tests in 0.000s

OK
```

# Cahier des charges

## Cahier des charges : vue d'ensemble

<!--%fig11 python log -->
![Code it with Python!](https://upload.wikimedia.org/wikipedia/commons/thumb/c/c3/Python-logo-notext.svg/1200px-Python-logo-notext.svg.png){width=150px}

* Concevoir la simulation WA-TOR en python. Produire un affichage console régulier et un graphique après n étapes avec `Pylab`
* Les fonctions sont regroupées dans des fichiers selon ce qu'elles font. Chaque fonction est testée et vérifiée.

## Cahier des charges : les fonctions à écrire

### Différentes ambitions selon les fonctions :

  * Données complètement
  * Seulement à commenter
  * Seulement à corriger
  * Seulement à tester
  * À écrire complètement

## Les rôles des trois participants

### Commun : Faire fonctionner l'ensemble

### 1. Initialiser la mer et les graphiques
### 2. Déplacer le poisson
### 3. Reproduction du poisson, mort du requin

## Difficultés

### Très inégalement répartie

* Faire fonctionner l'ensemble est le plus délicat.
* De nombreux phénomènes sont aléatoires (positions initiales, choix d'une case, présence de voisins particuliers etc.) et parfois délicats à tester.
* Le projet est relativement long et va demander du temps.
* Le déplacement est le plus complexe. De nombreuses étapes sont déjà franchies pour vous.
* Chaque équipier a une mission de difficulté comparable.

## Conclusion

### Au boulot !

<!-- fig13 requin -->
![Avec de l'enthousiasme !](./wator_diapos/requin_chasse.jpg){width=80%}
