---
  title:
    WA-TOR
  author:
    Quentin Konieczko (qu3nt1n@gmail.com)
---
# WA-TOR : présentation

## Simulation proie prédateur

### Introduction

Wa-Tor est une simulation de type proie-prédateur. Dans une mer torique évoluent des thons (les proies) et des requins (les prédateurs). Les uns et les autres se déplacent et se reproduisent. Pour acquérir l'énergie suffisante à sa survie un requin doit manger un thon régulièrement. Un thon vit éternellement tant qu'il n'est pas mangé par un requin.

---

**Remarque** : *une mer "torique" signifie qu'elle est représentée en machine par un tableau à deux dimensions où les bords sont reliés. Quand un poisson quitte la mer par la droite il apparaît à gauche. Quand un poisson quitte la mer par le bas il apparaît en haut.*


### Présentation des éléments de la simulation

#### La mer

La mer est représentée par une grille à deux dimensions torique. Chaque case a quatre voisines, une dans chacune des quatre directions cardinales (N, S, E, O). Chaque case de cette grille représente une zone de mer qui peut être soit vide, soit occupée par un thon ou un requin.

*Remarque importante pour la suite* : on respectera durant tout le projet cet ordre des directions : (N, S, E, O) = (haut, bas, gauche, droite)

#### Les poissons

* Chaque **thon** est caractérisé par son temps de gestation. Ce temps est initialisé à une valeur initiale commune à tous les thons, appelée durée de gestation des thons.
* Chaque **requin** est caractérisé par son temps de gestation et son énergie. Ces deux valeurs sont initialisées à une valeur initiale commune à tous les requins, appelées respectivement durée de gestation des requins et énergie des requins.



#### Simulation et comportement
À chaque pas de la simulation, une case de la mer est sélectionnée aléatoirement. Si elle est vide, il ne se passe rien. Si elle est occupée par un poisson, on applique alors son comportement. Les comportements de thons et des requins sont régis par des règles simples.

* Un thon applique le comportement suivant :

  1. Déplacement. Le thon choisit aléatoirement une case libre parmi ses voisines. S'il en existe une, le thon se déplace vers cette case. Il reste sur place sinon.

  2. Reproduction. Le temps de gestation du thon est diminué de 1.
    * Si ce temps arrive à 0, le thon donne naissance à un nouveau thon qui naît sur la case qu'il vient de quitter s'il s'est déplacé.
    * Sinon aucun thon ne naît. Le temps de gestation est remis à sa valeur initiale.

* Un requin applique le comportement suivant :

  1. Énergie. Le requin perd un point d'énergie.
  2. Déplacement. Le requin choisit aléatoirement parmi ses voisines une case occupée par un thon. S'il en existe une, le requin se déplace vers cette case et mange le thon. Son niveau d'énergie est alors remis à sa valeur initiale. Sinon il cherche à se déplacer vers une case voisine vide choisie au hasard. Il reste sur place s'il n'y en a aucune.
  3. Mort. Si le niveau d'énergie du requin est à 0, il meurt. Dans ce cas l'étape suivante n'a évidemment pas lieu.
  4. Reproduction. Le temps de gestation du requin est diminué de 1. Si ce temps arrive à 0, il donne naissance à un nouveau requin sur la case qu'il vient de quitter s'il s'est déplacé, sinon aucun requin ne naît. Son temps de gestation est remis à sa valeur initiale.

**Remarque** : On prendra garde à l'ordre dans lequel on applique ces comportements. Certains peuvent être échangés d'autres doivent être effectués à certains moments.

### Phénomènes proies-prédateurs émergents

Les durées de gestation des deux espèces et l'énergie récupérée par un requin lorsqu'il mange un thon sont des paramètres de la simulation. S'ils sont bien choisis on peut voir émerger un phénomène "périodique" d'évolution des populations.

Quand il y a peu de prédateurs, la population des proies augmente, mais cette abondance de proies permet alors aux prédateurs de facilement trouver l'énergie suffisante pour leur survie et leur reproduction. Leur population se met alors à croître au détriment de celle de leur proie. Quand ces dernières deviennent trop rares, les prédateurs finissent par mourir sans se reproduire. Et quand il y a peu de prédateurs...

Pour obtenir ce phénomène cyclique, il faut respecter les inégalités suivantes :

temps gestation des thons < énergie des requins < durée gestation des requins

Les valeurs suggérées sont 2, 3 et 5.

Il est également souhaitable de débuter avec une configuration dans laquelle il y a un nombre de thons relativement important par rapport au nombre de requins. Par exemple 30% des cases de la mer sont initialement occupées par des thons et 10% par des requins.

### Compléments

Si on veut avoir un affichage de l'évolution de la mer, il est peut être pertinent de ne pas afficher toutes les étapes mais une tous les 100 (par exemple) et de faire une petite pause après chaque affichage grâce au module time et sa fonction sleep :

```
from time import sleep
sleep(0.1) # pause de 1/10eme de seconde
```

On pourra ajouter à la simulation des variables globales qui comptabilisent à chaque instant les nombres de requins et de thons. On peut alors enregistrer à chaque pas de simulation le triplet (numéro du pas, nombre de thons, nombre de requins).

Le résultat du programme peut alors être la liste de ces triplets pour tous les pas. Les données collectées dans cette liste peuvent être utilisées pour dessiner les courbes des évolutions de population. On peut pour cela utiliser le module pylab.

Tracer une courbe avec pylab est assez simple. Il suffit de faire :

```
import pylab

data_x = *une liste d'abscisses*
data_y = *une liste d'ordonnées*
pylab.plot(data_x, data_y)
pylab.title('courbe des points de coordonnées (x,y)')
pylab.show()
```

Voici alors ce que l'on peut obtenir pour une mer de taille 10x10 et 10000 pas. On peut observer les cycles décalés d'évolution des populations (les thons sont en bleu et le requins en orange) :

![pylab](./wator_diapos/evolution.png)

# Le projet WA-TOR

## Les fichiers fournis au départ

Il est composé de plusieurs fichiers englobant les fonctions de chaque partie.

* Les fichiers **.py** sont de deux types :
  * des fonctions exécutées par le projet qui sont généralement incomplètes. Vous devez donc les compléter. Ce sera détaillé dans la partie suivante.
  * des fonctions dont le nom commence par *test_blablabla.py*. Ce sont des tests unitaires de chaque fonction. Ils sont déjà complets et vous pouvez les exécuter. Tous les tests échouent pour l'instant.

  Vous pouvez commenter les tests tant que vous développez une fonction. Cela clarifiera l'affichage.

* Le dossier **donnees_exemples** ne sert qu'aux tests.
  Ce sont des données générées pour tester différentes fonctions.

* le dossier **figs** présente différents résultats de graphiques similaires à ceux que vous construirez.

## Le résultat attendu à la fin

* Votre projet doit fonctionner convenablement.
  * Pas d'erreurs.
  * Affichages réguliers dans la consoles.
  * Affichage du graphique final.
* Chaque fonction doit être documentée. Les morceaux de code rendus obsolètes doivent être supprimés.
* Les tests fournis doivent tous passer avec succès.

Il est impossible de faire fonctionner le projet tant que toutes les fonctions ne sont pas programmées. Voilà pourquoi un jeu de test est fourni.

# Travail à effectuer

Nous allons décrire toutes les étapes à accomplir pour que le projet fonctionne.

## Remarque initiale

Le projet se découpe en 4 parties. La première est commune. Les trois suivantes sont propres à un équipier.

Le travail de l'équipier n°2 est plus long que les autres. Il recevra donc l'aide des deux autres une fois leurs tâches respectives accomplies.

# Partie commune

La partie commune à tous les équipiers comporte deux fichiers de code et tous les fichiers de tests.

## `constantes.py`

* `constantes.py` : remplir ce fichier avec les constantes définies plus tôt.

## `wa-tor.py`

Ce fichier fait fonctionner la simulation. Il nécessite tous les autres et ne pourra être testé qu'en dernier, quand tout sera réalisé.

Cela n'empêche pas de le compléter maintenant.

Il contient de nombreux imports, une fonction contenant une partie d'initialisation et une boucle.

### Consignes pour `wa_tor.py`

1. Compléter la documentation du fichier. Et étudier ce qui est déjà présent.
2. La boucle **`while`** doit tourner jusqu'à disparition d'une espèce ou jusqu'à avoir atteint le nombre d'étapes. Modifier la condition d'arrêt en conséquence.
3. Les trois tableaux `table_step`, `table_thons` et `table_requins` doivent être initialisés correctement.
4. Ajouter les nouvelles données des tableaux `table_thons` et `table_requins` à chaque étape de la simulation.
5. Incrémenter les étapes à chaque tour.



# Équipier n°1. Initialiser la mer et les graphiques

Voici les consignes pour le premier équipier.

Vous avez moins de fonctions à concevoir que l'équipier n°2. Une fois vos fonctions traitées, vous devrez l'aider.

## Initialiser la mer

L'objectif de cette partie est d'affecter dans un tableau une mer à l'état initial. Retrouver plus haut la description de la mer.

Tout ce travail est réalisé dans le fichier **generation_mer.py**

## Consignes pour generation_mer.py

Tous les imports sont déjà présents. Il n'est pas nécessaire d'en faire d'autres.

###  La fonction **`creerMerVide`**

La documentation est fournie mais pas le code.

Écrire le code de la fonction **`creerMerVide`**

### La fonction **`creerRequin`**

Elle est fournie complètement. Étudier cette fonction

### La fonction **`creerThon`**

Écrire un programme similaire à celui de **`creerRequin`**.

Documenter correctement cette fonction.

### La fonction **`verifierProbabilites`**

La fonction `verifierProbabilites` est presque complète.

On doit aussi s'assurer que les probabilités sont positives ou nulles (on doit pouvoir générer une mer avec une seule espèce).

Ajouter un test similaire aux précédents à la fin de cette fonction.

### La fonction **`creerMer`**

La fonction `creerMer` est celle qui va renvoyer la grille à l'état initial.

Elle comporte trois étapes qu'il faut programmer.
  1. Vérifier que les probabilités données conviennent.
  2. Affecter dans `grid` une mer vide.
  3. Pour chaque cellule de `grid`, tirer un nombre au hasard entre 0 et 1 avec `random()`.
    * S'il est plus petit que `proba_thon`, on ajoute un thon dans la cellule.
    * S'il est compris entre `proba_thon` et `proba_thon + proba_requin`, on ajoute un requin.
    Il faudra donc parcourir chaque ligne du tableau puis chaque cellule de cette ligne. Deux boucles imbriquées seront nécessaires.

Enfin, on renvoie la grille.

La documentation est fournie mais la code. Programmer cette fonction.


## Consignes pour **affichage.py**

Ce fichier est celui qui génère tous les affichages : ceux dans la console au fur et à mesure de l'avancement de la simulation et celui du graphique final.

Il contient aussi une fonction utilitaire qui compte les poissons dans une grille.

## Affichage dans la console.

Description du principe :

1. On crée une chaîne de caractères vide appelée **`board`**.
2. On parcourt la grille. Pour chaque case rencontrée on ajoute deux caractères : celui correspondant au poisson (ou un espace) et un espace.
  Cela nous permettra d'avoir un espace autour de chaque poisson présenté et rendra la lecture plus facile.
  À chaque fin de ligne on ajoute un retour à la ligne `\n`.
3. On affiche cette chaîne dans la console.
4. On affiche ensuite l'état global de la simulation (étapes et effectifs des espèces) dans la console avec des `print`.

### La fonction **`caractere_board`**
La fonction est complète mais n'a pas de documentation. Ajouter la documentation.

### La fonction **`genererBoard`**
La documentation est complète mais le code est manquant. Programmer cette fonction complètement.

### La fonction **afficherMer**
Le programme est complet mais il n'y a pas de documentation. Ajouter la documentation.

### La fonction **compterPoissons**
Cette fonction est utilisée régulièrement pour dénombrer les espèces. Programmer cette fonction et ajouter sa documentation.

## Affichage du graphique

### La fonction **construireGraphique**
Cette fonction utilise la librairie `pylab` pour afficher le graphique qui sera généré à la fin de la simulation.

La documentation est complète mais le code est manquant. Programmer cette fonction complètement.

### Tester la fonction **construireGraphique**

Cette fonction ne comporte pas de test unitaire parmi les fichiers de test.

Vous pouvez la tester avec la fonction **construireGraphiqueSimule** qui est fournie complète.



# Équipier n°2. Déplacer le poisson

Voici les consignes pour le second équipier.

Les autres équipiers (en particulier l'équipier n°3) ont moins de fonctions que vous à concevoir. Ils ont pour consigne de vous aider une fois leur tâche accomplie.

## Le déplacement d'un poisson

* Vous n'avez qu'un seul fichier à étudier et compléter : **deplacement.py**

* On a regroupé dans ce fichier trois parties du comportement : la chasse d'un requin, le déplacement d'un poisson vers une case vide (aussi pour le requin ne pouvant chasser) et la diminution d'énergie du requin.

* Tout ce qui concerne la mort du requin par épuisement et la reproduction est traité par le troisième équipier.

## Fonction `deplacement`

Toutes les étapes sont découpées en fonctions unitaires et c'est la fonction finale, **`deplacement`** qui coordonne l'ensemble.

Cette partie de la simulation comportant de nombreux cas elle est la plus délicate.

Commençons par la fonction **`deplacement`** qui dirige l'ensemble.

Une fois que vous l'aurez bien comprise, les différentes étapes sont faciles.

* Étudier cette fonction.
* Récupération d'informations. Expliquer en quelques lignes pourquoi on dispose de toutes les informations nécessaires.
* Initialisation des drapeaux
* Premier test (requin ?). Cette partie est complète
* Second test (le poisson a t-il chassé ?). Lire la documentation de la fonction **`deplacementSimplePoisson`** et programmer cette partie.
* Troisième test.
  * Si le test est vrai, on calcule les nouvelles coordonnées. Cette partie est complète.
  * Si le test est faux, on renvoie quand même les variables `nlle_abs` et `nlle_ord`. Compléter cette partie.

## Les autres fonctions du fichier **deplacement.py**

### la fonction **`voisinCase`**

La documentation est complète mais il manque deux valeurs au dictionnaire renvoyé.
Compléter le dictionnaire en ajoutant les valeurs manquantes.

### la fonction **`conserverNone`**

Il n'y pas de documentation dans cette fonction. Ajouter la documentation de la fonction.

### la fonction **`conserverThons`**

La documentation est fournie mais elle n'est pas programmée. Programmer cette fonction.

### la fonction **`choisirDirectionAleatoire`**

Cette fonction n'est pas programmée. Programmer cette fonction.
Se référer à la documentation de la fonction `choice` du module `random` si nécessaire.

### la fonction **`coordoneesDirection`**

* Expliquer le rôle du `% LARGEUR_MER` pour la variable `nlle_abs`.
* Il manque une variable à cette fonction. Programmer cette variable.

### la fonction **`chasseRequin`**

Elle est donnée complète. On se servira de cette fonction pour créer, de manière analogue, la fonction **`deplacementSimplePoisson`**

### la fonction **`deplacementSimplePoisson`**

Cette fonction est vide. Programmer cette fonction et rédiger sa documentation.



# Équipier n°3 : la mort par épuisement et la reproduction

Voici les consignes pour le troisième équipier.

C'est vous qui avez le moins de fonctions à concevoir. Une fois vos fonctions traitées, vous devez aider l'équipier n°2 (qui en a le plus).

## Mort par épuisement

On appelle cette fonction quand on a déjà déplacé le poisson. On ne connait pas l'espèce du poisson déplacé. Si le poisson est un requin et s'il est épuisé, on le tue.

### La fonction **`mortDuRequin`**

Une seule fonction, très simple. Elle ne renvoie rien.

La documentation est fournie, programmer cette fonction.

## Reproduction

Rappelons rapidement le principe de la reproduction d'un poisson.

Si la case choisie est un poisson, il perd un point de gestation.
Si son point de gestation est nul, il est réinitialisé.
Dans ce cas et si le poisson s'est déplacé, un nouveau poisson apparaît dans la case dont il est parti.

### La fonction **`reproduction`**

Il manque une partie de la documentation et tout le programme de la fonction.

* Se référer à la fonction **`generation_suivante`** pour choisir convenablement ce que doit faire votre fonction.

* Compléter la documentation de la fonction **`reproduction`**
* Programmer la fonction.
