# La présentation faite aux élèves

Ce dossier contient les sources des présentations qui seront données aux élèves.

## Format des sources.

Les sources sont écrites pour rendre un pdf lisible via pandoc + xetex etc.
C'est pour ça qu'il reste des `{wdith=50%}` à gauche et à droite.

## Introduction du projet

Dans un premier temps je projetterai un diaporama **introduction-diapos.pdf**

Durant cette présentation on abordera toutes les étapes de la simulation, 
introduction, motivation, mer torique, règles du comportement, phénomènes
proie prédateur etc.

## Travail élève


Il est accompagné d'un fichier **introduction-texte.pdf** qui est plus facile à
imprimer.

### Première partie


Dans ce fichier on présente d'abord sommairement le projet (afin de pouvoir 
se passer du fichier précédent par la suite) ensuite on réparti les rôles.

### Deuxième partie : cahier des charges

Ensuite les élèves seront répartis par groupe de trois et lirons avec moi le
fichier **travail_eleve.pdf**.


**Collectif** : Tout le monde commence par le fichier **wa-tor.py** afin de 
comprendre la structure
**Elève 1** : en charge de la creation de la mer initiale et des affichages
**Elève 2** : le déplacement d'un poisson
**Elève 3** : la mort des requins et la reproduction des poissons.

Volontairement, les parties sont de difficultés différentes. Les élèves ayant 
fini plus tôt ont pour consigne de se pencher sur ce qui reste (partie 2 
vraisemblablement). Cela forcera (je l'espère) l'esprit de groupe et les 
échanges.


