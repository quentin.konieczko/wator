# WA-TOR

par Quentin Konieczko


## Présentation

Ce dossier contient le travail à rendre pour le projet WA-TOR

L'organisation du projet répond à ce que je souhaite développer chez les élèves :

* le travail en équipe
* le respect des bonnes pratiques
* de multiples notions du programme de première (algorithmie, Python)

## Les dossiers

1. **présentation** contient les fichiers qui seront présentés. 
Une introduction et le cahier des charges détaillé
2. **project** la correction complète du projet. J'y détaille dans le readme les
choix effectués.
3. **project_eleves** le point de départ des élèves

## Choix pédagogiques

### Avant tout les contraintes sont les suivantes :

* Début du troisième trimestre
* Après avoir abordé tout le programme sur Python
* Après avoir réalisé un petit projet en groupe
* 1h de présentation suivie de Q/R
* 3 ou 4 séances de 2h sur poste
* groupes de 3 élèves

### Quels sont les écueils que je souhaite absolument éviter ?

* La désorganisation
* La stagnation
* Le découragement
* Le travail isolé et impossible à intégrer

### Point de départ des élèves

Considérant qu'on ne peut tout travailler à la fois, je fournis donc une
structure complète du projet. Elle est déjà très détaillée. 

**L'objectif n'est pas d'étudier comment dissequer un tel projet en de multiples
tâches très simples.**

Cela sera travaillé dans la tâche de fin d'année qui préparera au grand oral de 
l'année suivante.

Ce qui a conduit à effectuer les choix suivants :

* Le projet est découpé en fichiers qui contiennent des fonctions simples
* Chaque fonction est déjà présente dans le point de départ. C'est discutable 
mais je trouve ça plus raisonnable que d'espérer que les élèves y parviennent
eux même.
* Chaque fonction est soit documentée, soit fournie sans documentation, soit
fournie complète.
* Toutes les fonctions sont testées à part. Au départ, tous les tests échouent.
* Ils doivent donc utiliser la documentation pour répondre à un cahier des 
charges très précis.

### Consignes, communication, échange de documents

La gestion de multiples projets en parallèle pose problème.
Je compte donc faire faire ce projet à tous les élèves en même temps comme avant
dernier projet de l'année. Le dernier sera beaucoup moins détaillé, pas 
forcement plus complexe et ils auront (normalement) développé quelques bonnes
habitudes.

Les élèves doivent donc valider tous les tests proposés. Je compte les aider à
franchir les étapes individuelles et demander et faire faire une présentation
orale des résultats obtenus. Idéalement avec un autre collègue ne connaissant
pas le projet.

#### Première partie : collective

Une fois la présentation effectuée et les confusions levées, les élèves se
pencheront directement sur la fonction qui gère la simulation. Il n'y a pas
grand chose à faire, surtout comprendre.

#### Seconde partie : individuelle

Les étapes de la simulation sont découpées en trois grandes parties :

1. Créer la mer initiale et afficher dans la console, afficher le graphique
2. Déplacement des poissons (et chasse)
3. Reproduction des poissons, mort par épuisement


La seconde partie est plus longue que les autres. Les élèves ayant terminé leur 
partie en premier devront aider celui se penchant sur la seconde. Cela favorisa 
les échanges.

#### La communication entre les élèves 

C'est le seul moyen d'avancer. A cette période de l'année ils auront déjà une
bonne maîtrise des outils proposés (chat, mail, partage de documents etc.) et
du langage Python. 

Ils ne seront pas surpris de lire une longue documentation ni rebuté 
devant l'implémentation d'un algorithme simple.

## Justification des choix pédagogiques

* Le projet me semble trop long pour être traité entièrement par un élève seul 
élève
* Le projet présente à la fois des difficultés techniques (tableaux 2D, 
multiples étapes), affichage d'un tableau dans la console, dictionnaire (ou 
autre structure de données choisies) et des difficultés algorithmiques (dans 
quel ordre traiter les étapes du comportement ? Quelles sont celles communes et
celles spécifiques ?) etc.
* Le temps manquera forcement et j'aimerais pouvoir traiter un petit projet sur
chaque "grande partie" du programme.

J'ai donc préféré me pencher sur l'aspect collectif + respect du cahier des 
charges plus que sur le pur aspect "conception" qui demanderait trop de temps.

Qui plus est, l'utilisation massive de tests unitaires permet de programmer 
avec précision de petits morceaux de code et donne un sentiment de progression 
régulière. 

L'absence de progression régulière dans les projets ISN a toujours été une 
raison majeure de découragement.

Toutes ces raisons m'ont conduit à négliger totalement l'aspect algorithmique et
à imposer le format des données. Cet aspect sera simplement discuté durant la
présentation sans laisser de liberté aux élèves.


# Évaluation

Classique à celle d'un projet :

* Investissement durant les séances
* Qualité de travail rendu (code et documentation)
* Aptitude à passer les tests fournis
* Intégration des différentes parties
* Qualité de échanges entre les élèves (difficile à évaluer...)
* Eventuellement un oral collectif par groupe, selon le calendrier
