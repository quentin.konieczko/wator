#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Fonctions utilisées pour le déplacement d'un poisson

* voisinCase : renvoie les voisins d'une case
* conserverNone : conserve les directions des None parmi les voisins
* conserverThons : conserve les directions des thons parmi les voisins
* choisirDirectionAleatoire : choisit aléatoirement une direction parmi
    les possibles
* coordoneesDirection : renvoie les coordonnées d'une direction choisie depuis
    une case
* chasseRequin : effectue la chasse éventuelle d'un requin
* deplacementSimplePoisson : effectue le déplacement d'un poisson vers une case
    vide
* deplacement : effectue le déplacement complet d'un poisson choisi dans la mer
'''

from constantes import HAUTEUR_MER, LARGEUR_MER, ENERGIE_REQUIN, DIRECTIONS
from affichage import afficherMer
from random import choice

##############################################################
#####                                                    #####
#####             DEPLACEMENT D'UN POISSON               #####
#####                                                    #####
##############################################################


def voisinCase(grid, abs, ord):
    '''
    renvoie un un dict des cases voisines dans une grille à la position abs, ord
    les clés sont N, S, E, O
    les valeurs sont les contenus des cases dans ces directions.
    la grille est un tore donc :
        bord de droite et gauche reliés,
        bord du haut et bas reliés

    :param grid: (list of list), la mer
    :param abs: (int) l'abscisse
    :param ord: (int) l'ordonnée
    :return: (dict) le contenu de chaque direction depuis la case initiale.
    '''
    return {
        "N": grid[ord - 1][abs],
        "S": grid[(ord + 1) % HAUTEUR_MER][abs],
        "E": grid[ord][(abs + 1) % LARGEUR_MER],
        "O": grid[ord][abs - 1],
    }


def conserverNone(voisins):
    '''
    Renvoie la liste des directions des voisins qui sont vides.

    :param voisins: (dict) dictionnaire des 4 voisins de la cellule N, S, E, O
    :return: (list) liste des voisins vides

    >>> voisins = {"N": None, "S": None, "E": {"type":"requin"}, "O": None}
    >>> conserverNone(voisins)
    ["E"]
    '''
    none_voisins = []
    for direction in voisins:
        if voisins[direction] is None:
            none_voisins.append(direction)

    return none_voisins


def conserverThons(voisins):
    '''
    Renvoie la liste des directions des voisins qui sont des thons.

    :param voisins: (dict) dictionnaire des 4 voisins de la cellule N, S, E, O
    :return: (list) liste des voisins qui sont des thons

    >>> voisins = {"N": None, "S": None, "E": {"type":"thon"}, "O": None}
    >>> conserverNone(voisins)
    ["E"]
    '''
    thons_voisins = []
    for direction in voisins:
        if voisins[direction] is not None:
            if voisins[direction]["type"] is "thon":
                thons_voisins.append(direction)

    return thons_voisins


def choisirDirectionAleatoire(voisins_retenus):
    """
    Renvoie une direction vers une case vide
    Les voisins sont obtenus dans l'ordre NSEW
    La direction est obtenue à partir de cet ordre

    :param positions_retenues: (list) liste de directions
        parmi "N", "S", "E", "O"
    :return: (str) la direction retenue parmi "N", "S", "E", "O"
    """

    choix = choice(voisins_retenus)
    direction = DIRECTIONS[choix]
    return direction


def coordoneesDirection(abs, ord, direction):
    '''
    Renvoie les coordonnées (abs, ord) associées à une direction
    cela dépend des dimensions du tableau qu'on passe en paramaètre
    la direction est un vecteur parmi DIRECTIONS : (0, 1) etc.

    :param abs: (int) l'absisse d'une cellule
    :param ord: (int) l'ordonnée d'une cellule
    :param direction: (tuple) la direction souhaitée
    :return: (tuple) les coordonnées de la case visée
    '''
    # la case est vide on se déplace
    nlle_abs = (abs + direction[0]) % LARGEUR_MER
    nlle_ord = (ord + direction[1]) % HAUTEUR_MER
    return nlle_abs, nlle_ord


def chasseRequin(grid, poisson, voisins):
    '''
    Le requin peut-il chasser ?
    On renvoie les flags deplacement_possible et chasse_possible et la direction
    d'un thon voisin.
    Ils sont False, False, None s'il est impossible de chasser

    :param grid: (list of list) la mer
    :param poisson: (dict) le requin
    :param voisins: (list) les voisins du poisson
    :return: (dict) poisson (son énergie est reset s'il chasse)
    :return: (bool) deplacement_possible
    :return: (dict) chasse_possible
    :return: (tuple) direction (None si impossible de chasser)
    '''
    # on extrait le dictionnaire des voisins contenant un thon
    position_thon = conserverThons(voisins)

    deplacement_possible = False
    chasse_possible = False
    direction = None

    # peut-il chasser ?
    if len(position_thon) > 0:
        # le requin a au moins un thon parmi ses voisins et chasse
        deplacement_possible = True
        chasse_possible = True
        direction = choisirDirectionAleatoire(position_thon)
        # le requin qui a chassé récupère toute son énergie
        poisson["energie"] = ENERGIE_REQUIN
        # le requin mange le thon, leur nombre décroit

    return poisson, deplacement_possible, chasse_possible, direction


def deplacementSimplePoisson(grid, poisson, voisins):
    '''
    Le poisson peut-il se déplacer dans une case vide ?
    On renvoie le drapeau deplacement_possible et la direction d'une case vide
    parmi les cases voisines
    Ils sont False et None s'il est impossible de se déplacer normalement.

    :param grid: (list of list) la mer
    :param poisson: (dict) le poisson
    :return: (bool) deplacement_possible. Vrai s'il peut se déplacer, Faux sinon
    :return: (tuple) direction (None si impossible de se déplacer)
    '''
    # on extrait le dictionnaire des voisins contenant un None
    position_none = conserverNone(voisins)

    deplacement_possible = False
    direction = None

    if len(position_none) > 0:
        # le poisson a au moins une case vide parmi ses voisins et se déplace
        deplacement_possible = True
        direction = choisirDirectionAleatoire(position_none)

    return deplacement_possible, direction


def deplacement(grid, abs, ord):
    '''
    Calcule le déplacement d'un poisson selon son type

    :param grid: (liste de liste) la mer
    :param abs: (int) l'absisse de la case contenant le poisson
    :param ord: (int) l'ordonnée de la case contenant le poisson
    :return nlle_abs: (int) l'absisse de la case de déstination
    :return nlle_ord: (int) l'ordonnée de la case de déstination
    :return deplacement_possible: (bool) True si le poisson s'est déplacé, False
    sinon

    Le thon ne fait que se déplacer vers une case voisine libre si possible

    Les requins se déplacent et mangent un thon voisin s'il en existe
    s'il n'y a pas de thon le requin se déplace vers une case libre
    '''
    # on extrait les informations dont on a besoin
    poisson = grid[ord][abs]
    voisins = voisinCase(grid, abs, ord)

    # on initialise les drapeaux
    chasse_possible = False  # True seulement si c'est un requin qui chasse.
    deplacement_possible = False

    # Avant tout, les requins cherchent à chasser
    if poisson["type"] == "requin":
        # peut-il chasser ?
        poisson, deplacement_possible, chasse_possible, direction =\
            chasseRequin(grid, poisson, voisins)

    # si c'est un requin qui a chassé il ne doit pas se déplacer à nouveau
    if not chasse_possible:
        deplacement_possible, direction = deplacementSimplePoisson(
            grid, poisson, voisins
        )

    # on déplace finalement le poisson
    if deplacement_possible:
        nlle_abs, nlle_ord = coordoneesDirection(abs, ord, direction)
        grid[nlle_ord][nlle_abs] = poisson
        grid[ord][abs] = None

    else:
        # on aboutit ici quand il ne peut pas du tout se déplacer
        nlle_abs = abs
        nlle_ord = ord
        grid[ord][abs] = poisson

    return nlle_abs, nlle_ord, deplacement_possible


if __name__ == '__main__':
    pass
