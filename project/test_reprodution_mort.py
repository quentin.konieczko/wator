
import unittest
from reprodution_mort import *
from constantes import ENERGIE_REQUIN


class TestWator(unittest.TestCase):
    def test_mortDuRequin_cas1(self):
        # cas 1 le requin épuisé meurt
        grid = [
            [{
                "type": "requin", "gestation": 4,
                "energie": 1
            }, None],
            [None, None]
        ]
        poisson = grid[0][0]
        nlle_abs = 0
        nlle_ord = 0

        expected_final = [[None, None], [None, None]]

        mortDuRequin(grid, poisson, nlle_abs, nlle_ord)
        self.assertEqual(grid, expected_final)

    def test_mortDuRequin_cas2(self):
        # cas 2 le requin non épuisé perd 1 d'énergie
        grid = [
            [{
                "type": "requin", "gestation": 4,
                "energie": 2
            }, None],
            [None, None]
        ]
        poisson = grid[0][0]
        nlle_abs = 0
        nlle_ord = 0

        expected_final = [
            [{
                "type": "requin", "gestation": 4,
                "energie": 1
            }, None],
            [None, None]
        ]

        mortDuRequin(grid, poisson, nlle_abs, nlle_ord)
        self.assertEqual(grid, expected_final)

    def test_reproduction_cas_1(self):
        # cas 1 le poisson qui ne peut se reproduire perd 1 de gestation
        grid = [
            [{
                "type": "requin", "gestation": 4,
                "energie": 2
            }, {
                "type": "requin", "gestation": 4,
                "energie": 3
            }],
            [{
                "type": "requin", "gestation": 4,
                "energie": 4
            }, None]
        ]
        reproduction(grid, 0, 0, 0, 0, False)
        self.assertEqual(grid,
                         [
                             [{
                                 "type": "requin", "gestation": 3,
                                 "energie": 2
                             }, {
                                 "type": "requin", "gestation": 4,
                                 "energie": 3
                             }],
                             [{
                                 "type": "requin", "gestation": 4,
                                 "energie": 4
                             }, None]
                         ]
                         )

    def test_reproduction_cas_2(self):
        # cas 2 le poisson qui peut se reproduire
        grid = [
            [{
                "type": "requin", "gestation": 1,
                        "energie": 2
            }, None],
            [{
                "type": "requin", "gestation": 4,
                        "energie": 4
            }, None]
        ]

        reproduction(grid, 1, 0, 0, 0, True),
        self.assertEqual(
            grid,
            [
                [{
                    "type": "requin", "gestation": TEMPS_GESTATION["requin"],
                    "energie": 2
                }, {
                    "type": "requin", "gestation": TEMPS_GESTATION["requin"],
                    "energie": ENERGIE_REQUIN
                }],
                [{
                    "type": "requin", "gestation": 4,
                    "energie": 4
                }, None]
            ]
        )


if __name__ == '__main__':
    unittest.main()
