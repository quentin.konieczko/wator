# Projet WA-TOR

## Correction du projet

Ce dossier contient la correction du projet WATOR

## Exécution

~~~
$ python3 wa_tor_project.py
~~~

## Remarques

Par défaut on exécute la simulation dans une grille 20x20 avec les données
fournies sur le mattermost par Jean-Christophe Routier

On affiche dans la console toutes les 1000 étapes la mer et les effectifs.

La simulation s'arrête après 100000 étapes ou si une espèce disparaît.
Elle fait généralement apparaître une phénomène proie-prédateur. 
Parfois une espèce disparait.

Enfin on construit un graphique.

## Choix de données

* La mer est un tableau 2D.
* Les poissons sont des `dict` contenant un type et des données.
* Les cases vides sont des `None`
* Les voisins et les directions possibles sont des dictionnaires aussi
ils ont pour clés "N", "S" etc.
* Chaque fichier contient les fonctions relatives à cette partie.
* Toutes les fonctions (ou presque) sont testées dans un fichier à part avec la
librairie `unittest`

J'ai opté pour ces choix car je compte fortement diriger les élèves et ne vais
pas leur laisser le choix de la structure du programme.

L'objectif principal du projet est de développer le travail en équipe plus que 
la conception d'un programme complet. 

Ils seront donc contraints de communiquer régulièrement sur leur avancée afin de 
progresser dans le projet.
