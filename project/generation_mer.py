#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
Fonctions permettant la création de la mer

creerMerVide : initialise la mer avec des None
creerThon : renvoie le dictionnaire d'un thon
creerRequin : renvoie le dictionnaire d'un requin
verifierProbabilites : s'assure que les probabilités des thons et requins sont
cohérentes
creerMer : regroupe le tout. Initilise une mer vide et la peuple aléatoirement
selon les probabilités.
'''

from constantes import *
from random import random

##############################################################
#####                                                    #####
#####               GÉNÉRATION DE LA MER                 #####
#####                                                    #####
##############################################################


def creerMerVide():
    '''
    renvoie une grille vide contenant des None de largeur et hauteur données
    :param largeur: (int) largeur de la grille
    :param hauteur: (int) hauteur de la grille
    :return: (list) une liste 2D de largeur et hauteur données
    '''
    grid = []
    for x in range(HAUTEUR_MER):
        grid.append([None] * LARGEUR_MER)
    return grid


def creerThon():
    """
    renvoie les données d'un thon, sa gestation est une constante
    :return: (dict) un thon
    """
    return {"type": "thon", "gestation": TEMPS_GESTATION["thon"]}


def creerRequin():
    '''
    :return: (dict) un thon
    '''
    return {
        "type": "requin", "gestation": TEMPS_GESTATION["requin"],
        "energie": ENERGIE_REQUIN
    }


def verifierProbabilites(proba_thon, proba_requin):
    '''
    Vérifie la confirmité des probabilités données.
    Elles doivent être entre 0 et 1 et leur somme ne peut dépasser 1.
    Sinon on lève une exception ValueError

    :param proba_thon: (int, float) une probabilité
    :param proba_requin: (int, float) une probabilité
    :return: None
    :CU: lève une exception si les conditions ne sont pas remplies.
    '''
    if not isinstance(proba_thon, (int, float)) \
            or not isinstance(proba_requin, (int, float)):
        raise ValueError("les probabilités doivent être des nombres")
    if proba_thon + proba_requin > 1:
        raise ValueError("la somme des probas doit etre < 1")
    if proba_thon < 0 or proba_requin < 0:
        raise ValueError("les probas doivent etre positives")


def creerMer(proba_thon, proba_requin):
    '''
    Crée le tableau initial
    renvoie une table contenant des requins et des thons ou rien
    la proportion de requins et de thons est donnée par les parametres

    :param proba_thon: (float) entre 0 et 1 inclus
    :param proba_requin: (float) entre 0 et 1 inclus
                La somme de deux probas ne peut dépasser 1
    :return: (list of list)
    '''
    verifierProbabilites(proba_thon, proba_requin)

    # on crée une mer vide
    grid = creerMerVide()

    # on la remplit de poissons
    for ligne in grid:
        for i in range(LARGEUR_MER):
            random_number = random()
            if random_number < proba_thon:
                ligne[i] = creerThon()
            elif random_number < proba_thon + proba_requin:
                ligne[i] = creerRequin()

    return grid
