#!/usr/bin/env python
# -*- coding: utf-8 -*-

import unittest
from deplacement import *


class TestDeplacement(unittest.TestCase):

    def test_voisinCase(self):
        self.assertEqual(voisinCase([list(range(HAUTEUR_MER))]*LARGEUR_MER,
                                    0, 0),
                         [0, 0, 1, 19])
        self.assertEqual(voisinCase([list(range(HAUTEUR_MER))]
                                    * LARGEUR_MER, 12, 13), [12, 12, 13, 11])

    def test_extrairePositionsNone(self):
        voisins = [None, {
            "type": "thon", "gestation": 1
        }, None, None]
        self.assertEqual(extrairePositionsNone(voisins), [0, 2, 3])

    def test_choisirDirectionAleatoire(self):
        positions_retenues = [0]
        self.assertEqual(choisirDirectionAleatoire(positions_retenues),
                         (0, -1))
        positions_retenues = [1]
        self.assertEqual(choisirDirectionAleatoire(positions_retenues),
                         (0, 1))
        positions_retenues = [2]
        self.assertEqual(choisirDirectionAleatoire(positions_retenues),
                         (1, 0))
        positions_retenues = [3]
        self.assertEqual(choisirDirectionAleatoire(positions_retenues),
                         (-1, 0))
        positions_retenues = [1, 3]
        self.assertIn(choisirDirectionAleatoire(positions_retenues),
                      ((0, 1), (-1, 0)))

    def test_coordoneesDirection(self):
        self.assertEqual(coordoneesDirection(0, 0, (1, 1)), (1, 1))
        self.assertEqual(coordoneesDirection(0, 0, (-1, -1)), (19, 19))
        self.assertEqual(coordoneesDirection(0, 0, (-1, 1)), (19, 1))
        self.assertEqual(coordoneesDirection(0, 0, (1, -1)), (1, 19))

    def test_extraireVoisinsThons(self):
        voisins = [None, {
            "type": "thon", "gestation": 2
        }, None, None]
        self.assertEqual(extraireVoisinsThons(voisins), [1])

    def test_deplacement_cas_1(self):
        # cas 1 le thon qui se deplace
        grid = [
            [None, {
                "type": "thon", "gestation": 3
            }] + [{
                "type": "thon", "gestation": 3
            }] * (LARGEUR_MER - 2)
        ] + [[{
            "type": "thon", "gestation": 3
        }] * LARGEUR_MER] * (HAUTEUR_MER - 1)

        deplacement(grid, 1, 0)

        self.assertEqual(
            [
                [{
                    "type": "thon", "gestation": 3
                }, None] + [{
                    "type": "thon", "gestation": 3
                }] * (LARGEUR_MER - 2)
            ] + [[{
                "type": "thon", "gestation": 3
            }] * LARGEUR_MER] * (HAUTEUR_MER - 1),

            grid
        )

    def test_deplacement_cas_2(self):
        grid = [
            [{
                "type": "requin", "gestation": 4,
                "energie": 2
            }, {
                "type": "thon", "gestation": 3
            }] + [None] * (LARGEUR_MER - 2)
        ] + [[None] * LARGEUR_MER] * (HAUTEUR_MER - 1)

        deplacement(grid, 0, 0)

        self.assertEqual(
            [
                [None, {
                    "type": "requin", "gestation": 4,
                    "energie": 3
                }] + [None] * (LARGEUR_MER - 2)
            ] + [[None] * LARGEUR_MER] * (HAUTEUR_MER - 1),

            grid
        )

    def test_deplacement_cas_4(self):
        # cas 4 le thon qui ne peut pas se déplacer
        grid = [
            [{
                "type": "thon", "gestation": 3
            }] + [{
                "type": "requin", "gestation": 3,
                "energie": 3
            }] * (LARGEUR_MER - 1)
        ] + [[{
            "type": "requin", "gestation": 3,
            "energie": 3
        }] * LARGEUR_MER] * (HAUTEUR_MER - 1)

        deplacement(grid, 0, 0)
        self.assertEqual(grid,
                         [
                             [{
                                 "type": "thon", "gestation": 3
                             }] + [{
                                 "type": "requin", "gestation": 3,
                                 "energie": 3
                             }] * (LARGEUR_MER - 1)
                         ] + [[{
                             "type": "requin", "gestation": 3,
                             "energie": 3
                         }] * LARGEUR_MER] * (HAUTEUR_MER - 1))

    def test_deplacement_cas5(self):
        # cas 5 le requin qui ne peut pas se déplacer
        grid = [[{
            "type": "requin", "gestation": 3,
            "energie": 3
        }] * LARGEUR_MER] * (HAUTEUR_MER)

        deplacement(grid, 0, 0)
        self.assertEqual(grid,
                         [[{
                             "type": "requin", "gestation": 3,
                             "energie": 3
                         }] * LARGEUR_MER] * (HAUTEUR_MER)
                         )


if __name__ == '__main__':
    unittest.main()
