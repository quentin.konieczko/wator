#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
Constantes de WA-TOR
'''

##############################################################
#####                                                    #####
#####                     CONSTANTES                     #####
#####                                                    #####
##############################################################

LARGEUR_MER = 20
HAUTEUR_MER = 20

ENERGIE_REQUIN = 3

TEMPS_GESTATION = {"thon": 2, "requin": 5}

# DIRECTIONS = ((0, -1), (0, 1), (1, 0), (-1, 0))  # N, S, E, W
DIRECTIONS = {"N": (0, -1), "S": (0, 1), "E": (1, 0), "O": (-1, 0)}
