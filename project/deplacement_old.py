#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Fonctions utilisées pour le déplacement d'un poisson

* voisinCase : renvoie les voisins d'une case
* extrairePositionsNone : extrait les indices des None d'une liste
* extraireVoisinsThons : extrait les indices des thons parmi les voisins
* choisirDirectionAleatoire : choisit aléatoirement une direction parmi
    les possibles
* coordoneesDirection : renvoie les coordonnées d'une direction choisie depuis
    une case
* chasseRequin : effectue la chasse éventuelle d'un requin
* deplacementSimplePoisson : effectue le déplacement d'un poisson vers une case
    vide
* deplacement : effectue le déplacement complet d'un poisson choisi dans la mer
'''

from constantes import HAUTEUR_MER, LARGEUR_MER, ENERGIE_REQUIN, DIRECTIONS
from random import choice

##############################################################
#####                                                    #####
#####             DEPLACEMENT D'UN POISSON               #####
#####                                                    #####
##############################################################


def voisinCase(grid, abs, ord):
    '''
    renvoie une liste des cases voisines dans une grille à la position abs, ord
    les voisins sont N, S, E, W
    la grille est un tore donc :
        bord de droite et gauche reliés,
        bord du haut et bas reliés

    :param grid: (list of list), la mer
    :param abs: (int) l'abscisse
    :param ord: (int) l'ordonnée
    '''
    return [
        grid[ord - 1][abs],                             # N
        grid[(ord + 1) % HAUTEUR_MER][abs],             # S
        grid[ord][(abs + 1) % LARGEUR_MER],             # E
        grid[ord][abs - 1],                             # W
    ]


def extrairePositionsNone(voisins):
    '''
    renvoie les indices des voisins None dans la liste des voisins
    :param voisins: (list) liste des 4 voisins de la cellule N, S, E, W
    :return: (list) la liste des indices voisins qui sont des None

    >>> extrairePositionsNone([None, None, {"type":"requin"}, None])
    [0, 1, 3]
    '''
    voisins_none = []
    for indice, voisin in enumerate(voisins):
        if voisin == None:
            voisins_none.append(indice)
    return voisins_none


def extraireVoisinsThons(voisins):
    '''
    renvoie les indices des thons parmi les voisins

    :param voisins: (list) la liste des voisins d'une case
    :return: (list) la liste des indices de thons qui sont parmi les voisins.
    '''
    voisins_thon = []
    for indice, voisin in enumerate(voisins):
        if voisin is not None:
            if voisin["type"] == "thon":
                voisins_thon.append(indice)
    return voisins_thon


def choisirDirectionAleatoire(positions_retenues):
    """
    Renvoie une direction vers une case vide
    Les voisins sont obtenus dans l'ordre NSEW
    La direction est obtenue à partir de cet ordre

    :param positions_retenues: (list) liste d'indice
    :return: (int) un indice
    """

    choix = choice(positions_retenues)
    direction = DIRECTIONS[choix]
    return direction


def coordoneesDirection(abs, ord, direction):
    '''
    Renvoie les coordonnées (abs, ord) associées à une direction
    cela dépend des dimensions du tableau qu'on passe en paramaètre
    la direction est un vecteur parmi DIRECTIONS : (0, 1) etc.

    :param abs: (int) l'absisse d'une cellule
    :param ord: (int) l'ordonnée d'une cellule
    :param direction: (tuple) la direction souhaitée
    :return: (tuple) les coordonnées de la case visée
    '''
    # la case est vide on se déplace
    nlle_abs = (abs + direction[0]) % LARGEUR_MER
    nlle_ord = (ord + direction[1]) % HAUTEUR_MER
    return nlle_abs, nlle_ord


def chasseRequin(grid, poisson, voisins):
    '''
    Le requin peut-il chasser ?
    On renvoie les flags deplacement_possible et chasse_possible et la direction
    d'un thon voisin.
    Ils sont False, False, None s'il est impossible de chasser


    :param grid: (list of list) la mer
    :param poisson: (dict) le requin
    :param voisins: (list) les voisins du poisson
    :return: (dict) poisson (son énergie est reset s'il chasse)
    :return: (bool) deplacement_possible
    :return: (dict) chasse_possible
    :return: (tuple) direction (None si impossible de chasser)
    '''
    # peut-il chasser ?
    position_thon = extraireVoisinsThons(voisins)
    deplacement_possible = False
    chasse_possible = False
    direction = None

    if len(position_thon) > 0:
        # le requin a au moins un thon parmi ses voisins et chasse
        deplacement_possible = True
        chasse_possible = True
        direction = choisirDirectionAleatoire(position_thon)
        # le requin qui a chassé récupère toute son énergie
        poisson["energie"] = ENERGIE_REQUIN
        # le requin mange le thon, leur nombre décroit

    return poisson, deplacement_possible, chasse_possible, direction


def deplacementSimplePoisson(grid, poisson, voisins):
    '''
    Le poisson peut-il se déplacer dans une case vide ?
    On renvoie le drapeau deplacement_possible et la direction d'une case vide
    parmi les cases voisines
    Ils sont False et None s'il est impossible de se déplacer normalement.

    :param grid: (list of list) la mer
    :param poisson: (dict) le poisson
    :return: (bool) deplacement_possible. Vrai s'il peut se déplacer, Faux sinon
    :return: (tuple) direction (None si impossible de se déplacer)
    '''
    position_none = extrairePositionsNone(voisins)
    deplacement_possible = False
    direction = None

    if len(position_none) > 0:
        # le poisson a au moins une case vide parmi ses voisins et se déplace
        deplacement_possible = True
        direction = choisirDirectionAleatoire(position_none)

    return deplacement_possible, direction


def deplacement(grid, abs, ord):
    '''
    Calcule le déplacement d'un poisson selon son type

    :param grid: (liste de liste) la mer
    :param abs: (int) l'absisse de la case contenant le poisson
    :param ord: (int) l'ordonnée de la case contenant le poisson
    :return nlle_abs: (int) l'absisse de la case de déstination
    :return nlle_ord: (int) l'ordonnée de la case de déstination
    :return deplacement_possible: (bool) True si le poisson s'est déplacé, False
    sinon

    Le thon ne fait que se déplacer vers une case voisine libre si possible

    Les requins se déplacent et mangent un thon voisin s'il en existe
    s'il n'y a pas de thon le requin se déplace vers une case libre
    '''
    poisson = grid[ord][abs]
    voisins = voisinCase(grid, abs, ord)
    chasse_possible = False  # True seulement si c'est un requin qui chasse.
    deplacement_possible = False

    # Avant tout, les requins cherchent à chasser
    if poisson["type"] == "requin":
        # peut-il chasser ?
        poisson, deplacement_possible, chasse_possible, direction =\
            chasseRequin(grid, poisson, voisins)

    # si c'est un requin qui a chassé il ne doit pas se déplacer à nouveau
    if not chasse_possible:
        deplacement_possible, direction = deplacementSimplePoisson(
            grid, poisson, voisins
        )

    # on déplace finalement le poisson
    if deplacement_possible:
        nlle_abs, nlle_ord = coordoneesDirection(abs, ord, direction)
        grid[nlle_ord][nlle_abs] = poisson
        grid[ord][abs] = None

    else:
        # on aboutit ici quand il ne peut pas du tout se déplacer
        nlle_abs = abs
        nlle_ord = ord
        grid[ord][abs] = poisson

    return nlle_abs, nlle_ord, deplacement_possible


if __name__ == '__main__':
    # cas 1 le thon qui se deplace
    grid = [
        [None, {
            "type": "thon", "gestation": 3
        }] + [{
            "type": "thon", "gestation": 3
        }] * (LARGEUR_MER - 2)
    ] + [[{
        "type": "thon", "gestation": 3
    }] * LARGEUR_MER] * (HAUTEUR_MER - 1)

    deplacement(grid, 1, 0)

    assert [
        [{
            "type": "thon", "gestation": 3
        }, None] + [{
            "type": "thon", "gestation": 3
        }] * (LARGEUR_MER - 2)
    ] + [[{
        "type": "thon", "gestation": 3
    }] * LARGEUR_MER] * (HAUTEUR_MER - 1) == grid
