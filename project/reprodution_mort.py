#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
Fonctions utilisées pour la reproduction et la mort d'un poisson

* reproduction : effectue la reproduction complète d'un poisson (gestation,
    apparition d'un nouveau poisson)
* mortDuRequin : tue le requin d'énergie nulle.

'''

from constantes import TEMPS_GESTATION
from generation_mer import creerThon, creerRequin


##############################################################
#####                                                    #####
#####   REPRODUCTION D'UN POISSON ET MORT D'UN REQUIN    #####
#####                                                    #####
##############################################################


def reproduction(grid, abs, ord, nlle_abs, nlle_ord, deplacement_possible):
    '''
    La reproduction du poisson.
    Renvoie les nouveaux états après calcul.
    Si le temps de gestation est arrivé à 0 et qu'il s'est déplacé,
    le poisson se reproduit.

    :param grid: (list of list) la mer
    :param abs: (int) l'absisse de la case de départ
    :param ord: (int) l'ordonnée de la case de départ
    :param nlle_abs: (int) l'absisse de la case d'arrivée
    :param nlle_ord: (int) l'ordonnée de la case d'arrivée
    :param deplacement_possible: (bool) est-il possible de se déplacer ?
    :return: None
    '''
    poisson = grid[nlle_ord][nlle_abs]

    # tous les poissons perdent un point de gestation
    poisson["gestation"] -= 1

    if poisson["gestation"] == 0:
        # il se reproduit
        # on reset le temps de gestation
        poisson["gestation"] = TEMPS_GESTATION[poisson["type"]]

        # le poisson ne se reproduit que s'il a pu se déplacer
        if deplacement_possible:
            # le poisson se reproduit et leur nombre croit
            # Cela dépend de l'espèce, il faut tester
            if poisson["type"] == "requin":
                grid[ord][abs] = creerRequin()
            else:
                grid[ord][abs] = creerThon()
    return


def mortDuRequin(grid, poisson, nlle_abs, nlle_ord):
    '''
    Tue le requin dépourvu d'énergie
    Si le requin n'a plus d'énergie on le tue. Sa case devient un None.

    :param grid: (list of list)
    :param poisson: (dict) le poisson
    :param nlle_abs: (int) l'absisse d'arrivée du poisson
    :param nlle_ord: (int) l'ordonnée d'arrivée du poisson
    :return: None
    :SE: modifie grid et poisson en place
    '''
    if poisson["type"] == "requin":
        poisson["energie"] -= 1
        # mort ?
        if poisson["energie"] == 0:
            grid[nlle_ord][nlle_abs] = None
