#!/usr/bin/env python
# -*- coding: utf-8 -*-

import unittest
from generation_suivante import *
from generation_mer import creerMer
from affichage import compterPoissons


class TestGenerationSuivante(unittest.TestCase):
    def test_generationSuivante_cas1(self):
        # mer vide
        grid = []
        for i in range(HAUTEUR_MER):
            grid.append([None] * LARGEUR_MER)

        expected_grid = []
        for i in range(HAUTEUR_MER):
            expected_grid.append([None] * LARGEUR_MER)

        generationSuivante(grid)

        self.assertEqual(grid, expected_grid)

    def test_generationSuivante_cas2(self):
        # mer random
        # le nombre de poissons de chaque espèce ne peut évoluer de plus de 1

        grid = creerMer(0.3, 0.1)
        nb_thons_depart, nb_requins_depart = compterPoissons(grid)
        generationSuivante(grid)
        nb_thons_arrivee, nb_requins_arrivee = compterPoissons(grid)

        self.assertIn(abs(nb_thons_depart - nb_thons_arrivee), [0, 1])
        self.assertIn(abs(nb_requins_depart - nb_requins_arrivee), [0, 1])

    def test_generationSuivante_cas3(self):
        # mer pleine de thons
        # le nombre de poissons de chaque espèce ne peut évoluer du tout

        grid = creerMer(1, 0)
        nb_thons_depart, nb_requins_depart = compterPoissons(grid)
        generationSuivante(grid)
        nb_thons_arrivee, nb_requins_arrivee = compterPoissons(grid)

        self.assertEqual(nb_thons_depart, nb_thons_arrivee)
        self.assertEqual(nb_requins_depart, nb_requins_arrivee)

    def test_generationSuivante_cas4(self):
        # mer pleine de requins
        # le nombre de poissons de chaque espèce ne peut évoluer du tout

        grid = creerMer(0, 1)
        nb_thons_depart, nb_requins_depart = compterPoissons(grid)
        generationSuivante(grid)
        nb_thons_arrivee, nb_requins_arrivee = compterPoissons(grid)

        self.assertEqual(nb_thons_depart, nb_thons_arrivee)
        self.assertEqual(nb_requins_depart, nb_requins_arrivee)

    def test_generationSuivante_cas5(self):
        # mer pleine de requins mourant (énergie == 1)
        # le nombre de thon reste nul
        # le nombre de requins diminue de 1

        grid = creerMer(0, 1)
        for ligne in grid:
            for poisson in ligne:
                poisson["energie"] = 1
        nb_thons_depart, nb_requins_depart = compterPoissons(grid)

        generationSuivante(grid)
        nb_thons_arrivee, nb_requins_arrivee = compterPoissons(grid)

        self.assertEqual(nb_thons_depart, nb_thons_arrivee)
        self.assertEqual(nb_requins_depart - 1, nb_requins_arrivee)


if __name__ == '__main__':
    unittest.main()
