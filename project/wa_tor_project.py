#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
https://gitlab-fil.univ-lille.fr/quentin.konieczko/portail/tree/master/bloc1/wator



données :
mer torique
thons avec 1 paramètre : gestation
requins avec 2 paramètres : energie, gestation

La mer est un tableau 2D.

Chaque poisson est représenté par un dictionnaire avec son type et ses données.

La majorité des étapes sont découpées en fonction regroupées par catégorie.

Les étapes importantes sont :
* la génération de la mer
* le calcul d'une génération suivante :
    * déplacement des poissons
    * l'énergie des requins est intégrée à ces étapes
    * la mort des requins
    * reproduction des poissons
* une boucle contient toutes les étapes nécessaires à la génération suivante
    et à l'affichage


Pour simplifier le calcul d'une génération suivante on a regroupé ce qui était
commun :

* temps de gestation qui diminue de 1.
* déplacement vers une case (pour se mouvoir ou chasser)

et séparé ce qui est spécifique au requin :

* energie qui diminue de 1
* chasse
* mort par épuisement


Améliorations possibles :

TODO :
    * utiliser dictionnaire {N, S, E, W} pour les voisins
'''

from time import sleep

from generation_mer import creerMer
from affichage import compterPoissons, afficherMer, construireGraphique
from affichage import sauvegarderData
from generation_suivante import generationSuivante

##############################################################
#####                                                    #####
#####                     SIMULATION                     #####
#####                                                    #####
##############################################################


def simulation(delai=0.1, freq_affichage=10, total=100000,
               proba_thon=0.3, proba_requin=0.1):
    """
    Simule le comportement dans une mer.
    les nombres de requins et de thons sont calculés à partir des probas
    On continue jusqu'à atteindre total.
    Si total == inf, on s'arrête quand une des espèces disparait.

    A chaque étape les effectifs sont enregistrées dans des tableaux internes.

    En fin de simulation les effectifs sont affichés

    :param delai: (float) delai d'affichage avant de reprendre les calculs.
    :param freq_affichage: (int) on affiche à cette fréquence le tableau.
    :param total: (int ou inf), on continue jusqu'à ce nombre de générations
    :param proba_thon: (float) proportion de thons dans la mer initiale
    :param proba_requins: (float) proportion de requins dans la mer initiale
    :return: None
    """
    # on initialise les éléments
    mer = creerMer(proba_thon, proba_requin)
    nb_thons, nb_requins = compterPoissons(mer)

    # on crée les tableaux et variables suivant l'avancée
    step = 0
    table_steps = []
    table_thons = []
    table_requins = []

    # on continue jusqu'à disparition d'une espèce ou jusqu'à
    # atteindre le nombre d'étapes
    while nb_thons > 0 and nb_requins > 0 and step < total:

        # on remplit le tableau qui présentera les résultats
        table_steps.append(step)
        table_thons.append(nb_thons)
        table_requins.append(nb_requins)

        if step % freq_affichage == 0:
            # on affiche dans la console tous les "freq_affichage"
            afficherMer(mer, nb_thons, nb_requins, step)
            sleep(delai)

        # calculer la génération suivante
        generationSuivante(mer)
        nb_thons, nb_requins = compterPoissons(mer)
        step += 1

    # sauvegarderData(table_steps, table_thons, table_requins)
    construireGraphique(table_steps, table_thons, table_requins)


if __name__ == '__main__':
    # Affichage tous les 1000 étapes pour une mer "standard" jusqu'à disparition
    # d'une espèce ou 1000000 étapes

    simulation(freq_affichage=1000, total=100000)
