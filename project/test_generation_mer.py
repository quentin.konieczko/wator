#!/usr/bin/env python
# -*- coding: utf-8 -*-

import unittest
from generation_mer import *


class TestGenerationMer(unittest.TestCase):
    def test_creerMerVide(self):
        self.assertEqual(creerMerVide(), [[None] * LARGEUR_MER] * HAUTEUR_MER)

    def test_creerRequin(self):
        self.assertEqual(creerRequin(), {
            "type": "requin", "gestation": TEMPS_GESTATION["requin"],
            "energie": ENERGIE_REQUIN
        })

    def test_creerThon(self):
        self.assertEqual(creerThon(), {
            "type": "thon", "gestation": TEMPS_GESTATION["thon"]
        })

    def test_creerMer(self):
        # mer vide
        self.assertEqual(creerMer(0, 0), [[None] * LARGEUR_MER] * HAUTEUR_MER)
        # mer pleine de thons
        self.assertEqual(creerMer(1, 0), [[creerThon()] * LARGEUR_MER] * HAUTEUR_MER)
        # mer pleine de requins
        self.assertEqual(creerMer(0, 1), [[creerRequin()] * LARGEUR_MER] * HAUTEUR_MER)


if __name__ == '__main__':
    unittest.main()
